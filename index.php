<?php
  session_start();
  $current_points = 123;
  $actions = array(
    0 => array(
      'id' => 1,
      'title' => 'Coffee',
      'rest_time' => 1,
      'recovery_time' => 100,
      'points' => 10,
    ),
    1 => array(
      'id' => 2,
      'title' => 'Meat',
      'rest_time' => 1,
      'recovery_time' => 200,
      'points' => 20,
    ),
    2 => array(
      'id' => 3,
      'title' => 'Clock',
      'rest_time' => 1,
      'recovery_time' => 300,
      'points' => 30,
    ),
    3 => array(
      'id' => 4,
      'title' => 'Tornado',
      'rest_time' => 1,
      'recovery_time' => 400,
      'points' => 40,
    ),
  );

  //Restore score
  if (!isset($_SESSION['score'])) {
    $_SESSION['score'] = $current_points;
  }


  //Collect action identifiers
  //Пояснения: Сбор идентификаторов, используется простая глобальная переменная
  // так как при интерпретации скрипта эти данные больше изменится не могут
  // в отсутствии ооп, функция для сбора идентификаторов просто не требуется
  $ids = [];
  foreach($actions as $value) {
    array_push($ids, $value['id']);
  }

  //Пояснения: Получаем action по его идентификатору, функция удобнее хардкода
  // который помимо того что является плохой практикой, так же заставил бы нас
  // каждый раз его обновлять при изменениях в списке actions
  // break помогает не крутить цикл дальше необходимого и меняет поведение
  // без break, при наличии двух actions с одинаковыми id выбирался бы последний
  // а с break, будет выбираться первый найденный
  function get_action($id, $actions) {
    $result = [];
    foreach($actions as $action) {
      if($action['id'] == $id) {
        $result = $action;
        break;
      }
    }
    return $result;
  }

  //Check POST
  //Пояснения: $variable * 1 - упрошённое приведение типа к int/float
  if($_POST['action_id']) {
    $action_id = $_POST['action_id'] * 1;
    $response = '{ "status": "fail" }';

    //Prepare session
    if (!isset($_SESSION['history'])) {
      $_SESSION['history'] = [];
      foreach($ids as $value) {
        $_SESSION['history'][$value] = 0;
      }
    }

    //Prepare response
    //Пояснения: Если мы прошли на максимальную глубину меняем сообщение $response
    // в противном случае оставляем её с сообщением об ошибке тз описывает такое
    // поведение, однако это лишает пользователя\поддержку возможности разобратся
    // что произошло. Обработка исключений позволила бы в случае неполадок
    // корректно оповестить пользователя о случившемся/вести лог/решить проблему
    // оперативнее
    if(in_array($action_id, $ids)) {
      $action = get_action($action_id, $actions);
      $current_time = time();
      $rest_time = $current_time - $_SESSION['history'][$action_id];
      $recovery_time = $action['recovery_time'];

      if($rest_time >= $recovery_time) {
        $_SESSION['history'][$action_id] = $current_time;
        $_SESSION['score']+= $action['points'];
        $response = '{ "status": "ok" }';
      }
    }

    header ('Content-Type: application/json');
    echo $response;
    exit;
  }

//Пояснения: убран перенос строки перед doctype для полной корректности HTML
?><!doctype html>
<html lang="ru">
    <head>
        <!--
          До всякого рендера (в том числе <title>) объявляется кодировка.
          Возможные проблемы XSS уязвимости с применением кодировки UTF-7
        -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Clicker</title>
        <!-- Отсутствие favicon создаёт ошибку в консоли при первом заходе  -->
        <meta name="description" content="Click to get points!">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--
          Первым подключен normalize.css чтобы избавится от большинства различий
          в работе браузеров. Гипотетически добавляет баги (firefox focus),
          в текущей версии кода проблему не воспроизвести, но при расширении
          функционала необходимо это учитывать

          Стили располагаются в head чтобы избежать отображения контента без стилей
          Пока размер не велик - допустимо, при расширении функционала стоит
          применять различные техники, от минимизации до critical path
        -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <div id="main-area">
          <div class="score-row"><?=$_SESSION['score']?></div>
          <div class="items-row">
            <?php
              foreach($actions as $action) {

                //Пояснения: Если страница обновленна необходимо востановить
                // данные о занятости actions из сессии.
                // Вначале printf казался удачной идеей, хуже не делает, остался
                // В HTML нет типов данных - всё строка, используется %s
                $cooldown = $action['rest_time'];
                if (isset($_SESSION['history'])) {
                  $current_time = time();
                  $rest_time = $current_time - $_SESSION['history'][$action['id']];
                  $recovery_time = $action['recovery_time'];
                  if($rest_time < $recovery_time) {
                    $cooldown = $recovery_time - $rest_time;
                  }
                }

                printf("<div title='%s' data-cooldown='%s' data-id='%s' data-recoverytime='%s' data-points='%s' class='item'></div>", $action['title'], $cooldown, $action['id'], $action['recovery_time'], $action['points']);
              }
            ?>
          </div>
        </div>
        <!--
          После рендера подгружаются скрипты, не отнимая время на отрисовку.
          Минификация приветствуется, но на данном этапе не требуется (~ 3KB)
        -->
        <script src="js/main.js"></script>
    </body>
    <!--
      Пустая строка в конце документа для исключения некоторых ситуаций
      возникающих при операции слияния в некоторых системах контроля версий
    -->
</html>
