// Создание общего объекта и присвоение ему алиаса для удобной работы
// и отсуствия мусора в глобальной области видимости
var Clicker = $ = {};
Clicker.itemsRoot = document.querySelector('.items-row');
Clicker.items = $.itemsRoot.querySelectorAll('.item');
Clicker.scoreRoot = document.querySelector('.score-row');
// +X.innerHTML - упрощенное приведение типа к int/float
// Конструкция X || 0, предотвращает использования NaN в дальнейших вычислениях
Clicker.score = function() {
  return +$.scoreRoot.innerHTML || 0;
}

// Функции форматирования числа в время и обратно
// Удобно т.к. могут изменится требования к форматированию
// а так же легко рефакторить\оптимизировать\расширять
Clicker.toTime = function (num) {
  num = parseInt(num, 10);
  var hrs   = Math.floor(num / 3600);
  var min = Math.floor((num - (hrs * 3600)) / 60);
  var sec = num - (hrs * 3600) - (min * 60);

  var correct = function(time, delimiter) {
    if(time == 0) return "";
    else if(time < 10) return "0" + time + delimiter;
    return time + delimiter;
  }

  return correct(hrs, ":") + correct(min, ":") + correct(sec, "");
}
Clicker.toNum = function(str) {
  if(!str) return;
  var arr = str.split(':');
  var sec = arr[arr.length - 1] * 1;
  var min = arr.length > 1 ? arr[arr.length - 2] * 60 : 0;
  var hrs = arr.length > 2 ? arr[arr.length - 3] * 60 * 60 : 0;
  var dyz = arr.length > 3 ? arr[arr.length - 4] * 60 * 60 * 24 : 0;

  return sec + min + hrs + dyz;
}
// Используются data-атрибуты для более удобного перехода на dataset конструкции
// возможного при отказе от старых IE
Clicker.refresh = function(items) {
  for(var i = 0; i < items.length; i++) {
    var cooldown = $.toNum(items[i].getAttribute('data-cooldown'));

    if(cooldown > 0) {
      items[i].setAttribute('data-cooldown', $.toTime(cooldown - 1));
    } else {
      items[i].removeAttribute('data-cooldown');
    }
  }
}
// Один страшный обработчик, т.к. на данном этапе развития отдельные реализации
// xhr запросов не требуются. Однако код на грани читаемости, желателен рефакторинг
// при любом расширении/создании этого или подобного функционала
Clicker.itemsRoot.onclick = function(event) {
  var target = event.target;
  if (target.className != 'item' || target.hasAttribute('data-cooldown')) return;

  var request = new XMLHttpRequest();
  request.open('POST', './', true);
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  request.send('action_id=' + target.getAttribute('data-id'));
  request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
      var data = request.response != "" ? JSON.parse(request.response) : {};
      if(data.status && data.status == 'ok') {
        target.setAttribute('data-cooldown', $.toTime(target.getAttribute('data-recoverytime')));
        $.scoreRoot.innerHTML = $.score() + target.getAttribute('data-points') * 1;
      }
    }
  }
}

// Крутим функцию обновления всех счётчиков для их синхронности
// Возможная оптимизация - дроп таймера когда все счётчики обнулены
// И запуск таймера при использовании любого из actions
setInterval(function() {
  $.refresh($.items);
}, 1000);
// Пустая строка в конце документа для исключения некоторых ситуаций
// возникающих при операции слияния в некоторых системах контроля версий
